# pyTU58 Change Log

## 0.2

* Dump utility retries and pauses to let user clean heads.
* Has dumped a real tape.
* tu58retension renamed to tu58rewind.

## 0.1a1

Alpha version with somewhat working tape dump utility. Minimally tested.

## 0.0a0

Early developmental versions which are incomplete, insufficiently tested, and likely to change in compatibility-breaking ways.
