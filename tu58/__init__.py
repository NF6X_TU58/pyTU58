#!/usr/bin/env python3
#
########################################################################
# Copyright (C) 2019-2022 Mark J. Blair, NF6X
#
# This file is part of pyTU58.
#
#  pyTU58 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyTU58 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyTU58.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""API for DEC TU58 DECtape II tape drive"""

__all__ = ['drive', 'rsp', 'util']

__version__ = '0.2'             # Package version
__defport__ = '/dev/tu58'       # Default serial port path
__defbaud__ = 38400             # Default serial port baud rate

__copyright__ = 'Copyright (C) 2022 Mark J. Blair <nf6x@nf6x.net>'
__url__ = 'https://gitlab.com/NF6X_TU58/pyTU58'


from .drive import Drive
