#!/usr/bin/env python3
#
########################################################################
# Copyright (C) 2019-2022 Mark J. Blair, NF6X
#
# This file is part of pyTU58.
#
#  pyTU58 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyTU58 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyTU58.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

import serial
import math
from itertools import zip_longest
from . import rsp
from . import __defport__, __defbaud__

def _grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # _grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


class Drive:

    def __init__(self, port=__defport__, baudrate=__defbaud__,
                 maxunit=rsp.MAXUNIT, maxblock=rsp.MAXBLOCK, debug=False):
        """Open serial port and initialize connected TU58 drive.

        Serial port and baud rate have default values, but may be
        overridden (and commonly will need to be).

        Optionally change maximum unit number and/or maximum block
        number, for use with TU58 emulations which extend the protocol
        to allow larger storage capacity than real TU58 drives and
        cartridges.

        Does not support different baud rates for sending and
        receiving."""
        
        self._port = serial.Serial(port=port, baudrate=baudrate,
                                   timeout=rsp.OP_TIMEOUT)
        self._maxunit = maxunit
        self._maxblock = maxblock
        self._debug = debug
        self.status = None
        self.statmsg = None
        self.initdrive()


    def _print_packet(self, packet, message='packet'):
        """Print deciphered packet."""

        flag   = packet[0]
        opcode = None
        succ   = None

        print(f'{message}:')

        # Flag byte
        if flag in rsp.flagnames:
            flagname = rsp.flagnames[flag]
        else:
            flagname = 'UNKNOWN FLAG'
        print(f'  0x{flag:02X} Flag =               {flagname}')
        
        if flag == rsp.FLAG_CONTROL:
            if len(packet) != rsp.CTRL_PKT_LEN:
                raise RuntimeError('Malformed control packet'
                                   f' (length != {rsp.CTRL_PKT_LEN})')

            mbc  = packet[1]
            print(f'  0x{mbc:02X} Message Byte Count = {mbc}')

            if mbc != rsp.CTRL_PKT_MBC:
                raise RuntimeError(f'Message byte count {mbc}'
                                   f' != {rsp.CTRL_PKT_MBC}')

            opcode = packet[2]
            if opcode in rsp.opnames:
                opname = rsp.opnames[opcode]
            else:
                opname = 'UNKNOWN OPCODE'
            print(f'  0x{opcode:02X} Opcode =             {opname}')

            if opcode == rsp.OP_ENDPACKET:
                succ = packet[3]
                if succ in rsp.succnames:
                    succname = rsp.succnames[succ]
                else:
                    succname = 'UNKNOWN SUCCESS CODE'
                print(f'  0x{succ:02X} Success code =       {succname}')

                unit = packet[4]
                print(f'  0x{packet[4]:02X} Unit =               {unit}')

                switches = packet[5]
                print(f'  0x{packet[5]:02X}')

                seqnum = packet[6] | (packet[7] << 8)
                print(f'  0x{packet[6]:02X} Sequence number =    {seqnum}')
                print(f'  0x{packet[5]:02X}')

                bytecount = packet[8] | (packet[9] << 8)
                print(f'  0x{packet[8]:02X} Byte count =         {bytecount}')
                print(f'  0x{packet[9]:02X}')
 
                summary = packet[10] | (packet[11] << 8)
                print(f'  0x{packet[10]:02X} Summary status'
                      f' =     0x{summary:04X}')
                print(f'  0x{packet[11]:02X}')

            else:
                modifier = packet[3]
                print(f'  0x{modifier:02X} Modifier'
                      f' =           0x{modifier:02X}')

                unit = packet[4]
                print(f'  0x{packet[4]:02X} Unit =               {unit}')

                switches = packet[5]
                print(f'  0x{packet[5]:02X} Switches'
                      f' =           0x{switches:02X}')

                seqnum = packet[6] | (packet[7] << 8)
                print(f'  0x{packet[6]:02X} Sequence number =    {seqnum}')
                print(f'  0x{packet[7]:02X}')

                bytecount = packet[8] | (packet[9] << 8)
                print(f'  0x{packet[8]:02X} Byte count =         {bytecount}')
                print(f'  0x{packet[9]:02X}')
 
                blocknum = packet[10] | (packet[11] << 8)
                print(f'  0x{packet[10]:02X} Block number =       {blocknum}')
                print(f'  0x{packet[11]:02X}')
            
            checksum = packet[12] | (packet[13] << 8)
            if self._verify_checksum(packet):
                ckmsg = ''
            else:
                ckmsg = 'BAD CHECKSUM'

            print(f'  0x{packet[12]:02X} Checksum'
                  f' =           0x{checksum:04X} {ckmsg}')
            print(f'  0x{packet[13]:02X}')
            
            if opcode == rsp.OP_ENDPACKET:
                if (succ != rsp.SUCC_NORMAL) and (succ in rsp.succmsgs):
                    print(f'  {rsp.succmsgs[succ]}')

        elif flag == rsp.FLAG_DATA:
            if len(packet) < 4:
                raise RuntimeError('Malformed data packet (length < 4)')
            
            mbc  = packet[1]
            if len(packet) != mbc + 4:
                raise RuntimeError(f'Message byte count {mbc}'
                                   f' != packet length {len(packet)} + 4')
            print(f'  0x{mbc:02X} Message Byte Count = {mbc}')

            data = packet[2:-2]
            for chunk in [data[n:n+16] for n in range(0,len(data),16)]:
                print(f'       {bytes(chunk).hex(" ",1)}')

            checksum = packet[-2] | (packet[-1] << 8)
            if self._verify_checksum(packet):
                ckmsg = ''
            else:
                ckmsg = 'BAD CHECKSUM'

            print(f'  0x{packet[-2]:02X} Checksum'
                  f' =           0x{checksum:04X} {ckmsg}')
            print(f'  0x{packet[-1]:02X}')

        else:
            # More needed here?
            pass


    def _checkargs(self, unit, block, bytecount, specialaddress):
        """Check arguments against unit and block limits."""

        if not 0 <= unit <= self._maxunit:
            raise ValueError(f'Unit number {unit} not in valid range'
                             f' 0..{self._maxunit}')
        if specialaddress:
            mb = ((self._maxblock + 1) * rsp.SPECIAL_MULT) - 1
            bl = rsp.SPECIAL_LEN
        else:
            mb = self._maxblock
            bl = rsp.BLOCK_LEN

        if not 0 <= block <= mb:
            raise ValueError(f'Starting block {block} not in valid range'
                             f' 0..{mb}')
        endblock = block + math.floor(max(bytecount-1, 0) / bl)
        if not 0 <= endblock <= mb:
            raise ValueError(f'Ending block {endblock} not in valid range'
                             f' 0..{mb}')
                                 
        
    def _calc_checksum(self, subpacket):
        """Calculate checksum of byte array.

        Pass this function a short packet, i.e. not including the last
        two bytes. TU58 packet checksums are formed by adding successive
        little-endian 16 bit words with end-around carry."""

        checksum = 0
        for w in _grouper(subpacket, 2, 0):
            checksum = checksum + w[0] + (w[1]<<8)
            if checksum > 0xFFFF:
                checksum = (checksum + 1) & 0xFFFF
        return bytes([checksum & 0xFF, (checksum >> 8) & 0xFF])
                    

    def _verify_checksum(self, packet):
        """Verify checksum of packet.

        Returns: True if checksum is correct, False if incorrect."""
        
        checksum = self._calc_checksum(packet[:-2])
        if checksum != packet[-2:]:
            return False
        return True


    def _success(self, packet):
        """Determine whether packet indicates successful completion."""

        try:
            if packet[0] != rsp.FLAG_CONTROL:
                # Wrong flag byte
                self.status = None
                return False

            self.status = packet[3]
            if self.status in rsp.succmsgs:
                self.statmsg = rsp.succmsgs[self.status]
            else:
                self.statmsg = 'unknown status'

            if self.status not in rsp.successful:
                # Success not reported
                return False
            else:
                # Report success if checksum is good
                return self._verify_checksum(packet)

        except IndexError:
            # Packet was probably too short
            return False


    def _send_command(self, opcode, unit=0, block=0, bytecount=0,
                      maintenance=False,
                      specialaddress=False,
                      reducesens=False):
        """Send a command packet to the drive"""

        self._checkargs(unit, block, bytecount, specialaddress)

        if reducesens:
            # Reduce read amplifier sensitivity
            modifier = 0x01
        else:
            modifier = 0x00

        if specialaddress:
            # Special address mode: 128 byte blocks instead of 512
            modifier = modifier | 0x80

        if maintenance:
            # Maintenance mode: inhibit retries
            switches = 0x10
        else:
            # Normal mode: retries enabled
            switches = 0x00

        cmd = bytes([rsp.FLAG_CONTROL, rsp.CTRL_PKT_MBC, opcode,
                    modifier, unit, switches, 0, 0,
                    bytecount&0xFF, (bytecount>>8)&0xFF,
                    block&0xFF, (block>>8)&0xFF])
        cmd = cmd + self._calc_checksum(cmd)
        if self._debug:
            self._print_packet(cmd,f'{rsp.opnames[opcode]}'
                               f'({unit},{block},{bytecount}) command')
        self._port.write(cmd)


    def _send_data(self, data):
        """Send data blocks to the drive"""

        for datablock in [data[n:n+rsp.DATA_PKT_MBC]
                          for n in range(0, len(data), rsp.DATA_PKT_MBC)]:
            flag = self._port.read(1)
            if flag[0] != rsp.FLAG_CONTINUE:
                raise RuntimeError('Drive did not respond with continue flag')
            packet = bytes([rsp.FLAG_DATA, len(datablock)])
            packet = packet + datablock
            packet = packet + self._calc_checksum(packet)
            if self._debug:
                self._print_packet(packet,'written data block')
            self._port.write(packet)


    def _get_response(self, flag=None):
        """Get response packets from drive.

        Reads zero or more data packets followed by an end packet.
        Caller may supply the first byte in order to use this function
        to process an unexpected end packet, for example.

        Returns tuple of (response packet, data).
        Raises exception on various protocol errors."""

        if flag is None:
            response = self._port.read(1)
        else:
            response = flag

        data = bytes()

        while True:

            if response == bytes([rsp.FLAG_INIT]):
                # Protocol error! Init drive to shut up stream of
                # FLAG_INIT, then raise exception.
                self.initdrive()
                raise RuntimeError('Drive reported protocol error')

            elif response == bytes([rsp.FLAG_DATA]):
                # Data packet
                response = response + self._port.read(1)
                response = response + self._port.read(response[1]+2)
                if not self._verify_checksum(response):
                    raise RuntimeError('Bad checksum')
                data = data + response[2:-2]
                if self._debug:
                    self._print_packet(response, 'read data')

                # Read next flag byte and continue loop
                response = self._port.read(1)


            elif response == bytes([rsp.FLAG_CONTROL]):
                # End packet
                response = response + self._port.read(rsp.CTRL_PKT_LEN - 1)
                if len(response) != rsp.CTRL_PKT_LEN:
                    raise RuntimeError('Malformed end packet'
                                       f' (length != {rsp.CTRL_PKT_LEN})')
                elif response[1] != rsp.CTRL_PKT_MBC:
                    raise RuntimeError('Malformed end packet'
                                       f' (byte count != {rsp.CTRL_PKT_MBC})')
                elif response[2] != rsp.OP_ENDPACKET:
                    raise RuntimeError('Malformed end packet'
                                       f' (opcode != 0x{rsp.OP_ENDPACKET:02X})')
                if not self._verify_checksum(response):
                    raise RuntimeError('Bad checksum')

                if self._debug:
                    self._print_packet(response, 'end packet')

                return (response, data)

            else:
                raise RuntimeError('Unexpected response from drive')
            

    def initdrive(self):
        """Initialize the connected TU58 drive."""
        
        # Send break to TU58 to reset it
        self._port.send_break(rsp.BREAK_TIME)

        # Discard serial port buffer contents
        self._port.reset_output_buffer()
        self._port.reset_input_buffer()

        # Send two INIT commands (first is expected to be ignored)
        self._port.write(bytes([rsp.FLAG_INIT, rsp.FLAG_INIT]))

        # Expect a CONTINUE
        self._port.timeout = rsp.INIT_TIMEOUT
        expect   = bytes([rsp.FLAG_CONTINUE])
        response = self._port.read(1)
        self._port.timeout = rsp.OP_TIMEOUT
        if len(response) == 0:
            raise RuntimeError('Timeout during drive initialization')
        if response != expect:
            raise RuntimeError('Drive did not respond correctly to INIT')


    def bootstrap(self, unit):
        """Initialize the connected TU58 drive and use bootstrap command
        to return block 0."""
        
        self._checkargs(unit, 0, 0, False)

        # Send break to TU58 to reset it
        self._port.send_break(rsp.BREAK_TIME)

        # Discard serial port buffer contents
        self._port.reset_output_buffer()
        self._port.reset_input_buffer()

        # Send bootstrap sequence
        self._port.write(bytes([rsp.FLAG_INIT, rsp.FLAG_BOOTSTRAP, unit]))

        # Read bootblock
        data = self._port.read(rsp.BLOCK_LEN)
        return(data)
    

    def selftest(self):
        """Instruct unit to run its self-test and return status."""

        self._send_command(rsp.OP_DIAGNOSE)
        response = self._get_response()[0]
        return(self._success(response))


    def read(self, unit, block, bytecount, maintenance=False, reducesens=False,
             specialaddress=False):
        """Read data from TU58 drive unit.

        Optionally enable maintenance mode to disable retries on errors.
        Optionally read with reduced sensitivity.
        Optionally use special address mode to access 128 byte blocks."""
        
        self._send_command(rsp.OP_READ, unit, block, bytecount,
                           maintenance, specialaddress, reducesens)
        (response,data) = self._get_response()
        if not self._success(response):
            # Error handling could be improved.
            # Can we also include more details in the exception?
            raise RuntimeError('Read failed')
        return data


    def write(self, unit, block, data, verify=False,
              specialaddress=False):
        """Write data to TU58 unit.

        Optionally verify each written block after writing it.
        Optionally use special address mode to access 128 byte blocks."""
        
        self._send_command(rsp.OP_WRITE, unit, block, len(data),
                           specialaddress=specialaddress,
                           reducesens=verify)
        self._send_data(data)
        response = self._get_response()[0]
        if not self._success(response):
            # Error handling could be improved.
            # Can we also include more details in the exception?
            raise RuntimeError('Write failed')
    

    def position(self, unit, block, specialaddress=False):
        """Position the tape to the specified block number."""
        
        self._send_command(rsp.OP_POSITION, unit, block,
                           specialaddress=specialaddress)

        response = self._get_response()[0]
        return(self._success(response))


    def retension(self, unit):
        """Retension tape with rewind/unwind/rewind positioning operations."""
        
        if not self.position(unit, 0):
            return False
        if not self.position(unit, self._maxblock):
            return False
        return self.position(unit, 0)
