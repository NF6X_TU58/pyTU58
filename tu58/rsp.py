#!/usr/bin/env python3
#
########################################################################
# Copyright (C) 2019-2022 Mark J. Blair, NF6X
#
# This file is part of pyTU58.
#
#  pyTU58 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyTU58 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyTU58.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

# Timing parameters
BREAK_TIME     = 0.15       # A little over 2 chars at 150 baud
INIT_TIMEOUT   = 5.0        # Timeout for drive response to INIT
OP_TIMEOUT     = 60.0       # Per-packet timeout for most operations

# Assorted constants
BLOCK_LEN      = 512
SPECIAL_LEN    = 128
SPECIAL_MULT   = 4
CTRL_PKT_LEN   = 14
CTRL_PKT_MBC   = CTRL_PKT_LEN - 4
DATA_PKT_MBC   = 128
MAXUNIT        = 1
MAXBLOCK       = 511

# Flag byte
FLAG_DATA      = 0o001
FLAG_CONTROL   = 0o002
FLAG_INIT      = 0o004
FLAG_BOOTSTRAP = 0o010
FLAG_CONTINUE  = 0o020
FLAG_XON       = 0o021
FLAG_XOFF      = 0o023

flagnames = {
    FLAG_DATA:      'FLAG_DATA',
    FLAG_CONTROL:   'FLAG_CONTROL',
    FLAG_INIT:      'FLAG_INIT',
    FLAG_BOOTSTRAP: 'FLAG_BOOTSTRAP',
    FLAG_CONTINUE:  'FLAG_CONTINUE',
    FLAG_XON:       'FLAG_XON',
    FLAG_XOFF:      'FLAG_XOFF'
}

# Op Codes
OP_NOP       = 0o000
OP_INIT      = 0o001
OP_READ      = 0o002
OP_WRITE     = 0o003
OP_RSVD004   = 0o004
OP_POSITION  = 0o005
OP_RSVD006   = 0o006
OP_DIAGNOSE  = 0o007
OP_GETSTATUS = 0o010
OP_SETSTATUS = 0o011
OP_RSVD012   = 0o012
OP_RSVD013   = 0o013
OP_ENDPACKET = 0o100

opnames = {
    OP_NOP:       'OP_NOP',
    OP_INIT:      'OP_INIT',
    OP_READ:      'OP_READ',
    OP_WRITE:     'OP_WRITE',
    OP_RSVD004:   'OP_RSVD004',
    OP_POSITION:  'OP_POSITION',
    OP_RSVD006:   'OP_RSVD006',
    OP_DIAGNOSE:  'OP_DIAGNOSE',
    OP_GETSTATUS: 'OP_GETSTATUS',
    OP_SETSTATUS: 'OP_SETSTATUS',
    OP_RSVD012:   'OP_RSVD012',
    OP_RSVD013:   'OP_RSVD013',
    OP_ENDPACKET: 'OP_ENDPACKET'
}

# Success codes
SUCC_NORMAL        = 0o000      # Normal success
SUCC_RETRY         = 0o001      # Success but with retries
SUCC_FAIL_SELFTEST = 0o377      # Failed self test
SUCC_PARTIAL_OP    = 0o376      # Partial operation (end of medium)
SUCC_BAD_UNIT      = 0o370      # Bad unit number
SUCC_NO_CARTRIDGE  = 0o367      # No cartridge
SUCC_WRITE_PROT    = 0o365      # Write protected
SUCC_DATA_CHECK    = 0o357      # Data check error
SUCC_SEEK_ERROR    = 0o340      # Seek error (block not found)
SUCC_MOTOR_STOP    = 0o337      # Motor stopped
SUCC_BAD_OPCODE    = 0o320      # Bad op code
SUCC_BAD_BLOCKNUM  = 0o311      # Bad block number (>511)

succnames = {
    SUCC_NORMAL:        'SUCC_NORMAL',
    SUCC_RETRY:         'SUCC_RETRY',
    SUCC_FAIL_SELFTEST: 'SUCC_FAIL_SELFTEST',
    SUCC_PARTIAL_OP:    'SUCC_PARTIAL_OP',
    SUCC_BAD_UNIT:      'SUCC_BAD_UNIT',
    SUCC_NO_CARTRIDGE:  'SUCC_NO_CARTRIDGE',
    SUCC_WRITE_PROT:    'SUCC_WRITE_PROT',
    SUCC_DATA_CHECK:    'SUCC_DATA_CHECK',
    SUCC_SEEK_ERROR:    'SUCC_SEEK_ERROR',
    SUCC_MOTOR_STOP:    'SUCC_MOTOR_STOP',
    SUCC_BAD_OPCODE:    'SUCC_BAD_OPCODE',
    SUCC_BAD_BLOCKNUM:  'SUCC_BAD_BLOCKNUM'
}

succmsgs = {
    SUCC_NORMAL:        'Operation succeeded.',
    SUCC_RETRY:         'WARNING: Operation succeeded with retries.',
    SUCC_FAIL_SELFTEST: 'ERROR: Drive failed self test.',
    SUCC_PARTIAL_OP:    'WARNING: Partial operation (end of media).',
    SUCC_BAD_UNIT:      'ERROR: Bad unit number.',
    SUCC_NO_CARTRIDGE:  'ERROR: No cartridge in unit.',
    SUCC_WRITE_PROT:    'ERROR: Cartridge is write protected.',
    SUCC_DATA_CHECK:    'ERROR: Data check error.',
    SUCC_SEEK_ERROR:    'ERROR: Seek error.',
    SUCC_MOTOR_STOP:    'ERROR: Motor stalled.',
    SUCC_BAD_OPCODE:    'ERROR: Bad opcode.',
    SUCC_BAD_BLOCKNUM:  'ERROR: Bad block number.'
}

successful = [SUCC_NORMAL, SUCC_RETRY, SUCC_PARTIAL_OP]
warning    = [SUCC_RETRY, SUCC_PARTIAL_OP]

# Summary status flags
SUM_SPECIAL    = 0x8000     # Special condition (errors)
SUM_TRANSFER   = 0x4000     # Transfer error
SUM_MOTION     = 0x2000     # Motion error
SUM_LOGIC      = 0x1000     # Logic error

sumnames = {
    SUM_SPECIAL:  'SUM_SPECIAL',
    SUM_TRANSFER: 'SUM_TRANSFER',
    SUM_MOTION:   'SUM_MOTION',
    SUM_LOGIC:    'SUM_LOGIC'
}
