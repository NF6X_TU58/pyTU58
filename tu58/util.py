#!/usr/bin/env python3
#
########################################################################
# Copyright (C) 2022 Mark J. Blair, NF6X
#
# This file is part of pyTU58.
#
#  pyTU58 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyTU58 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyTU58.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

"""pyTU58 utility scripts"""

import argparse
import sys
from . import __version__, __copyright__, __url__
from . import __defport__, __defbaud__
from .drive import Drive
from . import rsp


########################################################################
# script support functions
########################################################################

def error_msg(msg, pause=False):
    """Print error message to stderr and optionally pause"""

    print(f"ERROR: {msg}", file=sys.stderr)
    sys.stderr.flush()
    if pause:
        print("Press return to continue...")
        sys.stdout.flush()
        input()


def warning_msg(msg, pause=False):
    """Print warning message to stderr"""

    print(f"WARNING: {msg}", file=sys.stderr)
    sys.stderr.flush()
    if pause:
        print("Press return to continue...")
        sys.stdout.flush()
        input()


def block_type(arg):
    """Parse block number argument"""

    try:
        b=int(arg)
        if not (0 <= b <= rsp.MAXBLOCK):
            raise ValueError()
    except ValueError:
        raise argparse.ArgumentTypeError(
            f"block number must be in range 0 to {rsp.MAXBLOCK}")
    return b


def unit_type(arg):
    """Parse unit number argument"""
    try:
        u=int(arg)
        if not (0 <= u <= rsp.MAXUNIT):
            raise ValueError()
    except ValueError:
        raise argparse.ArgumentTypeError(
            f"unit number must be in range 0 to {rsp.MAXUNIT}")
    return u


def nonnegativeint_type(arg):
    """Parse non-negative integer argument"""
    try:
        i=int(arg)
        if not i >= 0:
            raise ValueError()
    except ValueError:
        raise argparse.ArgumentTypeError('must be integer >= 0')
    return i


def add_common_args(parser):
    """Add common arguments to parser"""

    parser.add_argument('-p', '--port', default=__defport__,
        help=f'TU58 drive serial port (default: {__defport__})')

    parser.add_argument('-b', '--baud', type=int, default=__defbaud__,
        help=f'TU58 drive baud rate (default: {__defbaud__})')

    parser.add_argument('-u', '--unit', type=unit_type, default=0,
        help='unit number (default: 0)')

    parser.add_argument('-d', '--debug', action='store_true',
        help='print internal debugging messages')


def add_block_args(parser):
    """Add block related arguments to parser"""

    parser.add_argument('-s', '--start', type=block_type, default=0,
        help='start block number (default: 0)')

    parser.add_argument('-e', '--end', type=block_type, default=rsp.MAXBLOCK,
        help=f"end block number (default: {rsp.MAXBLOCK})")

    parser.add_argument('-P', '--pause', action='store_true',
        help='pause after errors/retries to allow head cleaning')

    parser.add_argument('-r', '--retry', metavar='RETRIES',
        type=nonnegativeint_type, default=1,
        help='retry count in special address mode (default: 1)')
    

########################################################################
# tu58dump script
########################################################################

def dump_main():
    """Main entry point for tu58dump script"""

    parser = argparse.ArgumentParser(
        prog = 'tu58dump',
        description = f"tu58dump {__version__}: Dump TU58 cartridge to file"
                      f"\n  {__copyright__}\n  {__url__}",
        add_help = True,
        formatter_class = argparse.RawDescriptionHelpFormatter)

    add_common_args(parser)
    add_block_args(parser)

    parser.add_argument('OUTFILE', type=argparse.FileType('wb'),
        help='output file')

    args = parser.parse_args()

    if args.end < args.start:
        raise argparse.ArgumentTypeError('end block must be >= start block')

    d = Drive(port=args.port, baudrate=args.baud, debug=args.debug)

    # Read each 512 byte logical block
    for blocknum in range(args.start, args.end+1):
        try:
            data = d.read(args.unit, blocknum, rsp.BLOCK_LEN)
            if (d.status == rsp.SUCC_RETRY):
                # Drive reported success with retries
                warning_msg(f"block {blocknum} succeeded with retries",
                            args.pause)

        except RuntimeError:
            # Drive reported error
            if args.retry > 0:
                rmsg = '(retrying)'
            else:
                rmsg = '(padding with 0xFF)'
            error_msg(f"block {blocknum} failed with error {d.statmsg} {rmsg}",
                      args.pause)

            if args.retry > 0:
                # Retry in special address mode to try to recover
                # 128 byte partial blocks
                data = b''
                for subblock in range(rsp.SPECIAL_MULT):
                    sbnum = (blocknum * rsp.SPECIAL_MULT) + subblock
                    for n in range(args.retry):
                        try:
                            subdata = d.read(args.unit, sbnum, rsp.SPECIAL_LEN,
                                             specialaddress = True)

                            if (d.status == rsp.SUCC_RETRY):
                                # Drive reported success with retries
                                warning_msg(f"block {blocknum}.{subblock}"
                                            f" succeeded with retries",
                                            args.pause)

                            # Successful read of sub-block:
                            # Concatenate sub-block and break out of
                            # retry loop
                            data = data + subdata
                            break

                        except RuntimeError:
                            if n < args.retry - 1:
                                rmsg = '(retrying)'
                            else:
                                rmsg = '(padding with 0xFF)'
                                data = data + b'\xFF' * rsp.SPECIAL_LEN

                            error_msg(f"block {blocknum}.{subblock} failed"
                                      f" with error {d.statmsg} {rmsg}",
                                      args.pause)


            else:
                # Not retrying. Pad output file with 0xFF
                data = b'\xFF' * rsp.BLOCK_LEN

        # write data to putput file and flush to disk
        args.OUTFILE.write(data)
        args.OUTFILE.flush()

    args.OUTFILE.close()


########################################################################
# tu58rewind script
########################################################################

def rewind_main():
    """Main entry point for tu58rewind script"""

    parser = argparse.ArgumentParser(
        prog = 'tu58retension',
        description = f"tu58rewind {__version__}:"
                      f" Rewind/retension TU58 cartridge"
                      f"\n  {__copyright__}\n  {__url__}",
        add_help = True,
        formatter_class = argparse.RawDescriptionHelpFormatter)

    add_common_args(parser)

    parser.add_argument('-r', '--retension', action='store_true',
        help='retension the tape with end-to-end seeks')

    args = parser.parse_args()

    d = Drive(port=args.port, baudrate=args.baud, debug=args.debug)

    if args.retension:
        if not d.retension(args.unit):
            error_msg(f"retension failed with status: {d.statmsg}")
            sys.exit(1)
    else:
        if not d.position(args.unit, 0):
            error_msg(f"rewind failed with status: {d.statmsg}")
            sys.exit(1)
